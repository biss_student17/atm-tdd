#include "Bank.h"

bool Bank::addBankAccount(BankAccount& account)
{
	std::ofstream outDBStream(DB_FILE, std::ios_base::app);

	if (findBankAccount(account.getId()) == nullptr)
	{
		outDBStream << account;
		outDBStream.close();
		return true;
	}
	outDBStream.close();
	return false;
}

size_t Bank::getAccountsLen() const
{
	std::ifstream inDBStream(DB_FILE, std::ios_base::app);
	BankAccount* account = new BankAccount;
	size_t accountlen = 0;

	for (accountlen; inDBStream >> *account; accountlen++);
	inDBStream.close();
	return accountlen;
}

BankAccount* Bank::findBankAccount(std::string id)
{
	getAccountsLen();
	std::ifstream inDBStream(DB_FILE, std::ios_base::app);
	BankAccount* account = new BankAccount;
	size_t accountlen = 0;
	for (accountlen; inDBStream >> *account; accountlen++)
	{
		if (account->getId() == id)
		{
			inDBStream.close();
			return account;
		}
	}
	inDBStream.close();
	return nullptr;
}

bool Bank::balanceTransfer(BankAccount& withdrawAccount, BankAccount& depositAccount, int64_t amount)
{
	if (withdrawAccount.withdraw(amount) && depositAccount.deposit(amount))
	{
		return true;
	}
	return false;
}
