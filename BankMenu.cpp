#include "BankMenu.h"

BankMenu::BankMenu()
{
	m_bank = new Bank();
}

BankMenu::~BankMenu()
{
}

void BankMenu::mainMenu()
{
	char option = 0;
	do
	{
		printf("\n%s", MAIN_MENU_TEXT);
		scanf("%c", &option);
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		switch (option)
		{
		case ADD_OPTION:
			addMenu();
			break;
		case FIND_OPTION:
			findMenu();
			break;
		case QUITE_OPTION:
			break;
		case DEPOSIT_OPTION:
			depositMenu();
			break;
		case WITHDRAW_OPTION:
			withdrawMenu();
		case TRANSFER_OPTION:
			transferMenu();
		default:
			printf("Invalid option\n");
			break;
		}
	} while (option != QUITE_OPTION);
}

void BankMenu::addMenu()
{
	std::string inputName;
	std::string inputId;
	BankAccount* newBankAccount;

	printf("%s", ADD_MENU_TEXT);

	std::getline(std::cin, inputName);
	std::getline(std::cin, inputId);

	newBankAccount = new BankAccount(inputName, inputId);

	if (m_bank->addBankAccount(*newBankAccount))
	{
		std::cout << inputId << ", bank account was added successfully" << std::endl;
	}
	else
	{
		std::cout << "Error, invalid account details" << std::endl;
	}
}

void BankMenu::findMenu()
{
	BankAccount* resultBankAccount;
	std::string inputId;

	printf("%s", FIND_MENU_TEXT);

	std::getline(std::cin, inputId);

	if ((resultBankAccount = m_bank->findBankAccount(inputId)) != nullptr)
	{
		std::cout << inputId << " bank account has " << resultBankAccount->getBalance() << " shmekels" << std::endl;
	}
	else
	{
		std::cout << "Error, no bank account under this id." << std::endl;
	}
}

void BankMenu::depositMenu()
{
	BankAccount* resultBankAccount;
	std::string inputId;
	int64_t inputAmount;

	printf("%s", DEPOSIT_MENU_TEXT);

	std::getline(std::cin, inputId);

	if ((resultBankAccount = m_bank->findBankAccount(inputId)) != nullptr)
	{
		if (getInputNum(inputAmount))
		{
			if(resultBankAccount->deposit(inputAmount))
			{
				std::cout << "Deposit to account passed successfully" << std::endl;
			}
			else
			{
				std::cout << "Error, invalid deposit amount" << std::endl;
			}
		}
	}
	else
	{
		std::cout << "Error, no bank account under this id." << std::endl;
	}
}

void BankMenu::withdrawMenu()
{
	BankAccount* resultBankAccount;
	std::string inputId;
	int64_t inputAmount;

	printf("%s", WITHDRAW_MENU_TEXT);

	std::getline(std::cin, inputId);

	if ((resultBankAccount = m_bank->findBankAccount(inputId)) != nullptr)
	{
		if (getInputNum(inputAmount))
		{
			if (resultBankAccount->withdraw(inputAmount))
			{
				std::cout << "Withdraw from account passed successfully" << std::endl;
			}
			else
			{
				std::cout << "Error, invalid withdraw amount" << std::endl;
			}
		}
	}
	else
	{
		std::cout << "Error, no bank account under this id." << std::endl;
	}
}

void BankMenu::transferMenu()
{
	BankAccount* withdrawBankAccount;
	BankAccount* depositBankAccount;
	std::string withdrawId;
	std::string depositId;
	int64_t inputAmount;

	printf("%s", TRANSFER_MENU_TEXT);

	std::getline(std::cin, withdrawId);
	if ((withdrawBankAccount = m_bank->findBankAccount(withdrawId)) != nullptr)
	{
		std::getline(std::cin, depositId);
		if ((depositBankAccount = m_bank->findBankAccount(depositId)) != nullptr)
		{
			if (getInputNum(inputAmount))
			{
				if (m_bank->balanceTransfer(*withdrawBankAccount, *depositBankAccount, inputAmount))
				{
					std::cout << "Transfer passed successfully" << std::endl;
				}
				else
				{
					std::cout << "Error, invalid amount" << std::endl;
				}
			}
		}
	}
	else
	{
		std::cout << "Error, no bank account under this id." << std::endl;
	}
}

bool BankMenu::getInputNum(int64_t& inputNum)
{
	std::cin >> inputNum;
	if (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore();
		std::cout << "Error, invalid number." << std::endl;
		return false;
	}
	return true;
}