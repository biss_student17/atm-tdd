#pragma once

#include <string>
#include <iostream>

using std::string;
constexpr size_t JOIN_GRANT_AMOUNT = 500;

class BankAccount
{
public:

	BankAccount() = default;
	BankAccount(string name, string id);
	~BankAccount();

	//---Getters & Setters---
	//Returns the bank account`s name.
	string getName() const;
	//Returns the bank account`s id.
	string getId() const;
	//Returns the bank account`s amount of money.
	int64_t getBalance() const;

	//Sets the bank account`s balance as given amount.
	void setBalance(int64_t balanceAmount);


	/*
	* Deposit given ammount into the bank account balance.
	* :param depositAmount:		The ammount to deposit into the bank account.
	* :return:					If the deposit was successful.
	*/
	bool deposit(int64_t depositAmount);

	/*
	* Withdraw given ammount from the bank account balance.
	* :param depositAmount:		The ammount to withdraw from the bank account.
	* :return:					If the withdraw was successful.
	*/
	bool withdraw(int64_t withdrawAmount);

	/*
	* Write the accounts variables to stream objects
	* :param out:			The stream object to write to.
	* :param account:		The account to add its variables to stream.
	* :return:				The updated stream.
	*/
	friend std::ostream& operator << (std::ostream& out, const BankAccount& account);

	/*
	* Read data from stream object and fill it in account variables.
	* :param in:			The The stream object to read from.
	* :param account:		The account to add its variables to stream.
	* :return:				The updated stream.
	*/
	friend std::istream& operator >> (std::istream& in, BankAccount& account);

private:
	string m_name;
	string m_id;
	int64_t m_balance;
};

