#pragma once

#include "Bank.h"
#include <stdio.h>
#include <iostream>

const char MAIN_MENU_TEXT[] = "Welcome to SHAULI & SONS bank\n"\
									"\ta - Add a new bank account.\n"\
									"\tf - Find existing bank account and prints stored money\n"\
									"\td - Deposit money into a bank account\n"\
									"\tw - Withdraw money from a bank account\n"\
									"\tt - Transfer money between a bank accounts\n"\
									"\tq - Quit the program\n";

const char ADD_MENU_TEXT[] = "Input full name and ID:\n";
const char FIND_MENU_TEXT[] = "Input ID to find:\n";
const char DEPOSIT_MENU_TEXT[] = "Input acount ID and ammount to deposit:\n";
const char WITHDRAW_MENU_TEXT[] = "Input acount ID and ammount to withdraw:\n";
const char TRANSFER_MENU_TEXT[] = "Input withdraw acount ID, deposit acount ID and ammount to transfer:\n";

const char ADD_OPTION = 'a';
const char FIND_OPTION = 'f';
const char QUITE_OPTION = 'q';
const char DEPOSIT_OPTION = 'd';
const char WITHDRAW_OPTION = 'w';
const char TRANSFER_OPTION = 't';


class BankMenu 
{
public:

	BankMenu();
	~BankMenu();

	
	//The main menu for the bank user,
	//the user can choose which bank action to make and to quite the program.
	void mainMenu();

protected:
	//The add menu for the bank user,
	//the user asked to insert full name and unique id, 
	//and gets notified if the add was successful or not.
	void addMenu();

	//The find menu for the bank user,
	//the user asked to insert bank account id to try to find in the bank,
	//if the bank account exists in the bank, the account balance will be printed,
	//and if not an error will be printed.
	void findMenu();

	//The deposit menu for the bank user,
	//the user asked to insert bank account id and amount to deposit,
	//if the bank account exists in the bank and the amount is valid the deposit will pass,
	//and if not an error will be printed.
	void depositMenu();

	//The withdraw menu for the bank user,
	//the user asked to insert bank account id and amount to withdraw,
	//if the bank account exists in the bank and the amount is valid the deposit will pass,
	//and if not an error will be printed.
	void withdrawMenu();

	//The transfer menu for the bank user,
	//the user asked to insert withdraw and deposit bank accounts id and amount to transfer,
	//if the bank accounts exists in the bank and the amount is valid the transfer will pass,
	//and if not an error will be printed.
	void transferMenu();

	/*
	* Gets user input and checks if its a int.
	* :param inputNum:		Refrence to a int to for user input.
	* :return:				If the user input was a valid int.
	*/
	bool getInputNum(int64_t& inputNum);

private:
	Bank* m_bank;
};