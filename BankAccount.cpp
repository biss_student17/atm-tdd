#include "BankAccount.h"


BankAccount::BankAccount(string name, string id): m_name(name), m_id(id), m_balance(JOIN_GRANT_AMOUNT)
{
}

BankAccount::~BankAccount()
{

}

string BankAccount::getName() const
{
	return m_name;
}

string BankAccount::getId() const
{
	return m_id;
}

int64_t BankAccount::getBalance() const
{
	return m_balance;
}

void BankAccount::setBalance(int64_t balanceAmount)
{
	m_balance = balanceAmount;
}

bool BankAccount::deposit(int64_t depositBalance)
{
	if (depositBalance >= 0)
	{
		m_balance += depositBalance;
		return true;
	}
	return false;
}

bool BankAccount::withdraw(int64_t withdrawAmount)
{
	if (withdrawAmount >= 0)
	{
		if (m_balance >= withdrawAmount)
		{
			m_balance -= withdrawAmount;
			return true;
		}
	}
	return false;
}

std::ostream& operator<<(std::ostream& out, const BankAccount& account)
{
	out << account.m_name << std::endl << account.m_id << std::endl << account.m_balance;
	return out;
}

std::istream& operator>>(std::istream& in, BankAccount& account)
{
	std::getline(in, account.m_name);
	in >> account.m_id;
	in >> account.m_balance;
	return in;
}