﻿#pragma once

#include "gtest/gtest.h"
#include "BankAccount.h"

#define TEST_SIZE INT16_MAX

class BankAccountTests : public ::testing::Test {
public:

    void SetUp() 
    {
        bankAccount = new BankAccount("Test Name", "123456789");
    }

    void TearDown() 
    {
        delete bankAccount;
    }

    BankAccount* bankAccount;
};
