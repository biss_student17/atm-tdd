#include "BankAccountTests.h"

TEST_F(BankAccountTests, getBalanceTest)
{
    ASSERT_EQ(bankAccount->getBalance(), JOIN_GRANT_AMOUNT);
}

TEST_F(BankAccountTests, getNameTest)
{
    ASSERT_EQ(bankAccount->getName(), "Test Name");
}

TEST_F(BankAccountTests, getIdTest)
{
    ASSERT_EQ(bankAccount->getId(), "123456789");
}

TEST_F(BankAccountTests, validDepositTest)
{
    for (int64_t validDepositAmount = 0; validDepositAmount < TEST_SIZE; validDepositAmount++)
    {
        int64_t beforeDeposit = bankAccount->getBalance();
        ASSERT_TRUE(bankAccount->deposit(validDepositAmount));
        ASSERT_EQ(bankAccount->getBalance(), validDepositAmount + beforeDeposit);
    }
}

TEST_F(BankAccountTests, invalidDepositTest)
{
    for (int64_t invalidDepositAmount = - 1; invalidDepositAmount > -TEST_SIZE; invalidDepositAmount--)
    {
        int64_t beforeDeposit = bankAccount->getBalance();
        ASSERT_FALSE(bankAccount->deposit(invalidDepositAmount));
        ASSERT_EQ(bankAccount->getBalance(), beforeDeposit);
    }
}

TEST_F(BankAccountTests, validWithdrawTest)
{
    for (int64_t validWithdrawAmount = 0; validWithdrawAmount <= TEST_SIZE; validWithdrawAmount++)
    {
        bankAccount->setBalance(TEST_SIZE);
        int64_t beforeWithdraw = bankAccount->getBalance();
        ASSERT_TRUE(bankAccount->withdraw(validWithdrawAmount));
        ASSERT_EQ(bankAccount->getBalance(), beforeWithdraw - validWithdrawAmount);
    }
}

TEST_F(BankAccountTests, invalidWithdrawTest)
{
    for (int64_t invalidWithdrawAmount = -1; invalidWithdrawAmount > -TEST_SIZE; invalidWithdrawAmount--)
    {
        bankAccount->setBalance(TEST_SIZE);
        ASSERT_FALSE(bankAccount->withdraw(invalidWithdrawAmount));
        ASSERT_EQ(bankAccount->getBalance(), TEST_SIZE);
    }
}

TEST_F(BankAccountTests, falseWithdrawTest)
{
    for (int64_t falseWithdrawAmount = JOIN_GRANT_AMOUNT + 1; falseWithdrawAmount < TEST_SIZE; falseWithdrawAmount++)
    {
        ASSERT_FALSE(bankAccount->withdraw(falseWithdrawAmount));
        ASSERT_EQ(bankAccount->getBalance(), JOIN_GRANT_AMOUNT);
    }
}