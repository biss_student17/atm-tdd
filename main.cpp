#include "gtest/gtest.h"
#include "stdio.h"
#include "BankAccountTests.h"
#include "BankMenu.h"

const bool isTesting = true;

int main(int argc, char** argv)
{
    if (isTesting)
    {
        ::testing::InitGoogleTest(&argc, argv);
        RUN_ALL_TESTS();
    }
    else
    {
        system("CLS");
        BankMenu* bank = new BankMenu();
        bank->mainMenu();
    }
    return 0;
}