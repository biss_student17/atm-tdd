#pragma once

#include <string>
#include <stdint.h>
#include <iostream>
#include <fstream>

#include "BankAccount.h"

const std::string DB_FILE = "accountsDB.txt";

class Bank
{
public:

	Bank() = default;
	~Bank() = default;

	/*
	* Adds given bank account to the bank accounts list.
	* :param account:	The account to add to the bank.
	* :return:			If the adding was successful.
	*/
	bool addBankAccount(BankAccount& account);

	/*
	* Returns the ammount of bank account in the bank.
	* :return:			The Amount of bank accounts.
	*/
	size_t getAccountsLen() const;

	/*
	* Search the account with the given id and returns it if found.
	* :param id:		The id`s bank account to search for.
	* :return:			The bank account with the id if found, if not returns a nullptr.
	*/
	BankAccount* findBankAccount(std::string id);

	/*
	* Transfers balance from given withdraw account to deposit account if amount is valid.
	* :param withdrawAccount:		The account to withdraw balance from.
	* :param depositAccount:		The account to deposit balance into.
	* :param amount:				The amount of money to transef.
	* :return:						If the transfer was successful.
	*/
	bool balanceTransfer(BankAccount& withdrawAccount, BankAccount& depositAccount, int64_t amount);

private:
};