#pragma once

#include "BankTests.h"


TEST_F(BankTests, getAccountsLenTest)
{
    ASSERT_EQ(bank->getAccountsLen(), 0);
    for (size_t accoutsIndex = 1; accoutsIndex < TEST_SIZE; accoutsIndex++)
    {
        ASSERT_TRUE(bank->addBankAccount(*(new BankAccount("Test Name", std::to_string(accoutsIndex)))));
        ASSERT_EQ(bank->getAccountsLen(), accoutsIndex);
    }
}

TEST_F(BankTests, addBankAccountTest)
{
    ASSERT_EQ(bank->getAccountsLen(), 0);
    for (size_t accoutsIndex = 0; accoutsIndex < TEST_SIZE; accoutsIndex++)
    {
        ASSERT_TRUE(bank->addBankAccount(*(new BankAccount("Test Name", std::to_string(accoutsIndex)))));
        ASSERT_EQ(bank->findBankAccount(std::to_string(accoutsIndex))->getName(), "Test Name");
        ASSERT_EQ(bank->findBankAccount(std::to_string(accoutsIndex))->getId(), std::to_string(accoutsIndex));
        ASSERT_EQ(bank->findBankAccount(std::to_string(accoutsIndex))->getBalance(), JOIN_GRANT_AMOUNT);
    }
}

TEST_F(BankTests, addDuplicateBankAccountTest)
{
    ASSERT_TRUE(bank->addBankAccount(*(new BankAccount("Test Name", "123456789"))));
    ASSERT_FALSE(bank->addBankAccount(*(new BankAccount("Test Name", "123456789"))));
}

TEST_F(BankTests, findBankAccountTest)
{
    ASSERT_TRUE(bank->addBankAccount(*(new BankAccount("Test Name", "123456789"))));
    BankAccount* resBankAccount = bank->findBankAccount("123456789");
    ASSERT_EQ(resBankAccount->getName(), "Test Name");
    ASSERT_EQ(resBankAccount->getId(), "123456789");
}

TEST_F(BankTests, falseFindBankAccountTest)
{
    BankAccount* resBankAccount = bank->findBankAccount("123456789");
    ASSERT_EQ(resBankAccount, nullptr);
}

TEST_F(BankTests, validBalanceTransferTest)
{
    BankAccount* firstBankAccount = new BankAccount("Test Name", "1");
    BankAccount* secondBankAccount = new BankAccount("Test Name", "2");
    for (size_t validTransferAmount = 0; validTransferAmount < TEST_SIZE; validTransferAmount++)
    {
        firstBankAccount->setBalance(TEST_SIZE);
        secondBankAccount->setBalance(TEST_SIZE);

        ASSERT_TRUE(bank->balanceTransfer(*firstBankAccount, *secondBankAccount, validTransferAmount));
        ASSERT_EQ(firstBankAccount->getBalance(), TEST_SIZE - validTransferAmount);
        ASSERT_EQ(secondBankAccount->getBalance(), TEST_SIZE + validTransferAmount);
    }
}

TEST_F(BankTests, invalidBalanceTransferTest)
{
    BankAccount* firstBankAccount = new BankAccount("Test Name", "1");
    BankAccount* secondBankAccount = new BankAccount("Test Name", "2");
    for (size_t invalidTransferAmount = -1; invalidTransferAmount > -TEST_SIZE; invalidTransferAmount--)
    {
        ASSERT_FALSE(bank->balanceTransfer(*firstBankAccount, *secondBankAccount, invalidTransferAmount));
        ASSERT_EQ(firstBankAccount->getBalance(), JOIN_GRANT_AMOUNT);
        ASSERT_EQ(secondBankAccount->getBalance(), JOIN_GRANT_AMOUNT);
    }
}

TEST_F(BankTests, falseBalanceTransferTest)
{
    BankAccount* firstBankAccount = new BankAccount("Test Name", "1");
    BankAccount* secondBankAccount = new BankAccount("Test Name", "2");
    for (size_t falseTransferAmount = JOIN_GRANT_AMOUNT + 1; falseTransferAmount < TEST_SIZE; falseTransferAmount++)
    {
        ASSERT_FALSE(bank->balanceTransfer(*firstBankAccount, *secondBankAccount, falseTransferAmount));
        ASSERT_EQ(firstBankAccount->getBalance(), TEST_SIZE - falseTransferAmount);
        ASSERT_EQ(secondBankAccount->getBalance(), TEST_SIZE + falseTransferAmount);
    }
}