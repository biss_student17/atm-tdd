#pragma once

#include "gtest/gtest.h"
#include <string>
#include "Bank.h"

#define TEST_SIZE INT8_MAX

class BankTests : public ::testing::Test {
public:

    void SetUp()
    {
        //Clears the file for the test.
        std::ofstream outDBStream(DB_FILE, std::ofstream::out | std::ofstream::trunc);
        outDBStream.close();
        bank = new Bank();
    }

    void TearDown()
    {
        delete bank;
    }
    Bank* bank;
};

